import React from "react";

export const default_sortingColumns = { 1: { direction: "asc", priority: 1 } };

export const handleSortingColumns = (sortable, getSortingColumns) => {
  return (value, { columnIndex }) => {
    const sortingColumns = getSortingColumns() || [];

    return (
      <div style={{ display: "inline" }}>
        <span className="value">{value}</span>
        {React.createElement(
          "span",
          sortable(value, {
            columnIndex
          })
        )}
        {sortingColumns[columnIndex] ? (
          <span className="sort-order">
            {sortingColumns[columnIndex].position
              ? sortingColumns[columnIndex].position + 1
              : 1}
          </span>
        ) : (
          <span className="sort-order" />
        )}
      </div>
    );
  };
};

export const columns = sortableHeader => {
  return [
    {
      property: "id",
      header: {
        label: "id",
        formatters: [sortableHeader]
      }
    },
    {
      property: "name",
      header: {
        label: "Name",
        formatters: [sortableHeader]
      }
    },
    {
      property: "family",
      header: {
        label: "family",
        formatters: [sortableHeader]
      }
    },
    {
      property: "city",
      header: {
        label: "city",
        formatters: [sortableHeader]
      }
    },
    {
      property: "score",
      header: {
        label: "score",
        formatters: [sortableHeader]
      }
    }
  ];
};
