import * as actionTypes from "../actions/actionTypes";

const initialState = {
  userInfos: null,
  error: false
};

export const updateObject = (oldObject, updatedProperties) => {
  return {
    ...oldObject,
    ...updatedProperties
  };
};

const setUserInfos = (state, action) => {
  return updateObject(state, {
    userInfos: [...action.userInfos],
    error: false
  });
};

const fetchDateFailed = (state, action) => {
  return updateObject(state, { error: true });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case actionTypes.SET_USERINFO:
    return setUserInfos(state, action);
  case actionTypes.FETCH_DATA_FAILED:
    return fetchDateFailed(state, action);
  default:
    return state;
  }
};

export default reducer;
