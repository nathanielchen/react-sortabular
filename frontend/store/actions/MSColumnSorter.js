import * as actionTypes from "./actionTypes";
import axios from "../../axios-orders";

export const setUserInfo = userInfos => {
  return {
    type: actionTypes.SET_USERINFO,
    userInfos: [...userInfos]
  };
};

export const fetchDateFailed = () => {
  return {
    type: actionTypes.FETCH_DATA_FAILED
  };
};

export const initData = () => {
  return dispatch => {
    axios
      .get("https://nate-277a7.firebaseio.com/.json")
      .then(response => {
        dispatch(setUserInfo(response.data));
      })
      .catch(error => {
        dispatch(fetchDateFailed());
      });
  };
};
