import React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";
import MSColumnSorter from "../components/MSColumnSorter/MSColumnSorter";
// import renderer from "react-test-renderer";

import {
  default_sortingColumns,
  handleSortingColumns,
  columns
} from "../shared/sort_utility";

describe("<CompanyName />", () => {
  it("<MSColumnSorter> should render Table Provider", () => {
    const wrapper = shallow(
      <MSColumnSorter
        columns={columns}
        sortingColumns={default_sortingColumns}
        handleSortingColumns={handleSortingColumns}
      />
    );

    expect(wrapper.prop("columns"));
    expect(wrapper.prop("sortingColumns"));
    expect(wrapper.prop("default_sortingColumns"));
  });
});
