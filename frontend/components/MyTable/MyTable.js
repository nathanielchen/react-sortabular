import React from "react";
import { compose } from "redux";
import orderBy from "lodash/orderBy";
import * as resolve from "table-resolver";
import * as Table from "reactabular-table";
import * as sort from "sortabular";

const rows = [
  { id: 1, name: "jack", family: "hanson", city: "sydney", score: 100 },
  { id: 2, name: "peter", family: "street", city: "melbourne", score: 200 },
  { id: 3, name: "joe", family: "larson", city: "brisbane", score: 300 },
  { id: 4, name: "simon", family: "long", city: "perth", score: 400 },
  { id: 5, name: "abraham", family: "blue", city: "darwin", score: 500 }
];

export default class MyTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rows: rows, // initial rows
      sortingColumns: { name: { direction: "asc", priority: 1 } }, // Defaultre sorting column
      columns: this.getColumns() // initial columns
    };
  }
  componentDidMount() {}

  sortHeader(sortable, getSortingColumns) {
    return (value, { columnIndex }) => {
      const sortingColumns = getSortingColumns || {};

      let returnValues = (
        <div style={{ display: "inline" }}>
          <span className="value">{value}</span>
          {React.createElement(
            "span",
            sortable(
              value,
              {
                columnIndex
              },
              {
                style: { float: "right" }
              }
            )
          )}
          {sortingColumns[columnIndex] && (
            <span
              className="sort-order"
              style={{ marginLeft: "0.5em", float: "right" }}
            >
              {sortingColumns[columnIndex].position + 1}
            </span>
          )}
        </div>
      );
      return returnValues;
    };
  }

  getColumns() {
    const sortable = sort.sort({
      getSortingColumns: () => this.state.sortingColumns || {},
      onSort: selectedColumn => {
        this.setState({
          sortingColumns: sort.byColumns({
            sortingColumns: this.state.sortingColumns,
            selectedColumn
          })
        });
      }
    });
    const sortableHeader = this.sortHeader(
      sortable,
      () => this.state.sortingColumns
    );

    return [
      {
        property: "id",
        header: {
          label: "id",
          formatters: [sortableHeader]
        }
      },
      {
        property: "name",
        header: {
          label: "Name",
          formatters: [sortableHeader]
        }
      },
      {
        property: "family",
        header: {
          label: "family",
          formatters: [sortableHeader]
        }
      },
      {
        property: "city",
        header: {
          label: "city",
          formatters: [sortableHeader]
        }
      },
      {
        property: "score",
        header: {
          label: "score",
          formatters: [sortableHeader]
        }
      }
    ];
  }

  render() {
    const { columns, sortingColumns } = this.state;
    const composed = compose(
      sort.sorter(
        { columns: columns, sortingColumns, sort: orderBy },
        resolve.resolve({
          columns: columns,
          method: resolve.nested
        })
      )
    )(this.state.rows);

    return (
      <Table.Provider
        className="table table-striped table-bordered"
        columns={columns}
      >
        <Table.Header />
        <Table.Body rows={composed} rowKey="id" />
      </Table.Provider>
    );
  }
}
