import React from "react";
import { compose } from "redux";
import orderBy from "lodash/orderBy";
import * as resolve from "table-resolver";
import * as Table from "reactabular-table";
import * as sort from "sortabular";

const rows = [
  { id: 1, name: "jack", family: "hanson", city: "sydney", score: 100 },
  { id: 2, name: "peter", family: "street", city: "melbourne", score: 200 },
  { id: 3, name: "joe", family: "larson", city: "brisbane", score: 300 },
  { id: 4, name: "simon", family: "long", city: "perth", score: 400 },
  { id: 5, name: "abraham", family: "blue", city: "darwin", score: 500 }
];

const columns = [
  {
    property: "id",
    header: {
      label: "id"
      // formatters: [sortableHeader]
    }
  },
  {
    property: "name",
    header: {
      label: "Name"
      // formatters: [sortableHeader]
    }
  },
  {
    property: "family",
    header: {
      label: "family"
      // formatters: [sortableHeader]
    }
  },
  {
    property: "city",
    header: {
      label: "city"
      // formatters: [sortableHeader]
    }
  },
  {
    property: "score",
    header: {
      label: "score"
      // formatters: [sortableHeader]
    }
  }
];

export default class MyTable extends React.Component {
  componentDidMount() {}

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Table.Provider
        className="table table-striped table-bordered"
        columns={columns}
      >
        <Table.Header />
        <Table.Body rows={rows} rowKey="id" />
      </Table.Provider>
    );
  }
}
