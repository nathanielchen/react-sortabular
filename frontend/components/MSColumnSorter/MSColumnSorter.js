import React from "react";
import { compose } from "redux";
import orderBy from "lodash/orderBy";
import * as resolve from "table-resolver";
import * as Table from "reactabular-table";
import * as sort from "sortabular";
import PropTypes from "prop-types";

const initial_rows = [
  { id: 1, name: "jack", family: "hanson", city: "sydney", score: 100 },
  { id: 2, name: "peter", family: "street", city: "melbourne", score: 200 },
  { id: 3, name: "joe", family: "larson", city: "brisbane", score: 300 },
  { id: 4, name: "simon", family: "long", city: "perth", score: 400 },
  { id: 5, name: "abraham", family: "blue", city: "darwin", score: 500 }
];

class MSColumnSorter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rows: initial_rows, // initial rows
      sortingColumns: this.props.sortingColumns, // Defaultre sorting column
      columns: this.getColumns() // initial columns
    };
  }

  getColumns() {
    const sortable = sort.sort({
      getSortingColumns: () => this.state.sortingColumns || {},
      onSort: selectedColumn => {
        this.setState({
          sortingColumns: sort.byColumns({
            sortingColumns: this.state.sortingColumns,
            selectedColumn
          })
        });
      }
    });
    const sortableHeader = this.props.handleSortingColumns(
      sortable,
      () => this.state.sortingColumns
    );

    return this.props.columns(sortableHeader);
  }

  render() {
    const { columns, rows, sortingColumns } = this.state;
    const composed = compose(
      sort.sorter(
        { columns: columns, sortingColumns, sort: orderBy },
        resolve.resolve({
          columns: columns,
          method: resolve.nested
        })
      )
    )(rows);

    return (
      <Table.Provider
        className="table table-striped table-bordered"
        columns={columns}
      >
        <Table.Header />
        <Table.Body rows={composed} rowKey="id" />
      </Table.Provider>
    );
  }
}

MSColumnSorter.propTypes = {};

export default MSColumnSorter;
